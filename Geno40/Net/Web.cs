﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;


[assembly: CLSCompliant(false)]
namespace Geno40.Net
{
    public static class Web
    {


        public static string GetWebUrlResponse(string url, string method, int timeout = -1)
        {
            try
            {
                string result = "";
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = method;
                if (timeout > 0)
                {
                    myRequest.Timeout = timeout;
                }
                using (HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string htmlResponse = "";
                        using (System.IO.StreamReader streamReader = new StreamReader(response.GetResponseStream(), true))
                        {
                            htmlResponse = streamReader.ReadToEnd().ToString();
                        }
                        result = htmlResponse;
                    }
                }
                return result;
            }
            catch
            {
                throw;
            }
        }
    }
}