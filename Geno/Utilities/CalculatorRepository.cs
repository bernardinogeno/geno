﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geno.Utilities
{
    public static class CalculatorRepository
    {
        public static int ComputeAge(DateTime birthday, DateTime dateToday)
        {
            if (birthday == null)
            {
                throw new ArgumentNullException("birthDate", "birthDate should not be null");
            }
            if (dateToday == null)
            {
                throw new ArgumentNullException("dateToday", "dateToday should not be null");
            }
            int age = dateToday.Year - birthday.Year;
            if (birthday > dateToday.AddYears(-age))
            { age--; }
            return age;
        }
    }
}
