﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Geno.Utilities
{
    public static class EncryptionRepository
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Sha", Justification = "It is an imported library")]
        public static string GetSha1HashData(string data)
        {
            try
            {
                //create new instance of md5
                byte[] hashData;
                using (SHA1 sha1 = SHA1.Create())
                {
                    //convert the input text to array of bytes
                    hashData = sha1.ComputeHash(Encoding.Default.GetBytes(data));
                }
                //create new instance of StringBuilder to save hashed data
                StringBuilder returnValue = new StringBuilder();
                //loop for each byte and add it to StringBuilder
                for (int i = 0; i < hashData.Length; i++)
                {
                    returnValue.Append(hashData[i].ToString(CultureInfo.InvariantCulture));
                }
                // return hexadecimal string
                return returnValue.ToString();
            }
            catch
            {
                throw;
            }
        }
    }
}
