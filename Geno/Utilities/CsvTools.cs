﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Geno.ExtensionMethods;
using System.Globalization;

namespace Geno.Utilities
{
    public static class CsvTools
    {
        /*http://stackoverflow.com/questions/2306667/how-can-i-convert-a-list-of-objects-to-csv*/
        public static string Escape(string data)
        {
            StringBuilder builder = new StringBuilder();
            bool needQuotes = false;
            foreach (char c in data.ToArray())
            {
                switch (c)
                {
                    case '"': builder.Append("\\\""); needQuotes = true; break;
                    case ' ': builder.Append(" "); needQuotes = true; break;
                    case ',': builder.Append(","); needQuotes = true; break;
                    case '\t': builder.Append("\\t"); needQuotes = true; break;
                    case '\n': builder.Append("\\n"); needQuotes = true; break;
                    default: builder.Append(c); break;
                }
            }
            if (needQuotes)
                return "\"" + builder.ToString() + "\"";
            else
                return builder.ToString();
        }

        /*http://stackoverflow.com/questions/2306667/how-can-i-convert-a-list-of-objects-to-csv*/
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1062:Validate arguments of public methods", MessageId = "0", Justification = "Parameter is being validated."), System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is a library.")]
        public static string ToCsv(ICollection<ICollection<string>> rows)
        {
            #region Parameters Validations
            if (rows.IsNullOrEmpty())
            {
                return "";
            }
            foreach (var columns in rows)
            {
                if (columns.IsNullOrEmpty())
                {
                    return "";
                }
            }
            #endregion

            StringBuilder toBeReturned = new StringBuilder();
            foreach (List<string> columns in rows)
            {
                string temp = "";
                foreach (string item in columns)
                {
                    temp += Escape(item);
                    temp += ",";
                }
                if (temp.Length > 0)
                {
                    temp = temp.Substring(0, temp.Length - 1);
                }
                temp += "\n";
                toBeReturned.Append(temp);
            }
            return toBeReturned.ToString();
        }

        /*http://www.codeproject.com/Tips/565920/Create-CSV-from-JSON-in-Csharp*/
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1720:IdentifiersShouldNotContainTypeNames", MessageId = "object", Justification = "Microsoft's library used expect a type object thus expecting the same for this method.")]
        public static string ToCsv(object csvObject, string delimiter)
        {
            XmlNode xml = JsonConvert.DeserializeXmlNode("{records:{record:" + JsonConvert.SerializeObject(csvObject) + "}}");
            XmlDocument xmldoc = new XmlDocument();
            //Create XmlDoc Object
            xmldoc.LoadXml(xml.InnerXml);
            //Create XML Steam 
            using (var xmlReader = new XmlNodeReader(xmldoc))
            {
                using (DataSet dataSet = new DataSet())
                {
                    dataSet.Locale = CultureInfo.InvariantCulture;
                    //Load Dataset with Xml
                    dataSet.ReadXml(xmlReader);
                    return ToCsv(dataSet.Tables[0], delimiter);
                }
            }
        }

        /*http://www.codeproject.com/Tips/565920/Create-CSV-from-JSON-in-Csharp*/
        public static string ToCsv(this DataTable table, string delimitor)
        {
            if (table == null)
            {
                return "";
            }
            var result = new StringBuilder();
            for (int i = 0; i < table.Columns.Count; i++)
            {
                result.Append(table.Columns[i].ColumnName);
                result.Append(i == table.Columns.Count - 1 ? "\n" : delimitor);
            }
            foreach (DataRow row in table.Rows)
            {
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    result.Append(row[i].ToString());
                    result.Append(i == table.Columns.Count - 1 ? "\n" : delimitor);
                }
            }
            return result.ToString().TrimEnd(new char[] { '\r', '\n' });
            //return result.ToString();
        }
    }
}
