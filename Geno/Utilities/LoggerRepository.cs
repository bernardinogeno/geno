﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geno.Utilities
{
    public static class LoggerRepository
    {
        public static Models.AzureStorageLogger CreateAzureStorageLogger(string connectionString)
        {
            return new Models.AzureStorageLogger(connectionString);
        }

        public static Models.AzureStorageLogger CreateAzureStorageLogger(string storageAccountName, string storageAccountKey)
        {
            return new Models.AzureStorageLogger(storageAccountName, storageAccountKey);
        }      
    }
}
