﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace Geno.Models
{
    public class AzureStorageLogger
    {
        private CloudStorageAccount storageAccount;
        private const string defaultQueueName = "default";
        private const string defaultTableName = "default";

        public AzureStorageLogger(string connectionString)
        {
            try
            {
                storageAccount = CloudStorageAccount.Parse(connectionString);
            }
            catch { throw; }
            if (storageAccount == null)
            {
                throw new ArgumentException("Invalid storage credentials");
            }
        }

        public AzureStorageLogger(string storageName, string storageKey)
        {
            try
            {
                storageAccount = new CloudStorageAccount(new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(storageName, storageKey), true);
            }
            catch { throw; }
            if (storageAccount == null)
            {
                throw new ArgumentException("Invalid storage credentials");
            }
        }

        public async Task LogToQueueAsync(object message, string queueName)
        {
            try
            {
                string serializedMessage = new JavaScriptSerializer().Serialize(message);
                await LogToQueueAsync(serializedMessage, queueName);
            }
            catch { throw; }
        }

        public async Task LogToQueueAsync(string message, string queueName)
        {
            if (string.IsNullOrEmpty(message))
            {
                throw new ArgumentNullException("message");
            }
            if (string.IsNullOrEmpty(queueName))
            {
                queueName = defaultQueueName;
            }
            try
            {
                CloudQueueClient queueClient = storageAccount.CreateCloudQueueClient();
                CloudQueue queue = queueClient.GetQueueReference(queueName);
                await queue.CreateIfNotExistsAsync();
                CloudQueueMessage queueMessage = new CloudQueueMessage(message);
                await queue.AddMessageAsync(queueMessage);
            }
            catch { throw; }
        }

        public async Task LogToTableAsync(TableEntity entity, string tableName)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            if (string.IsNullOrEmpty(tableName))
            {
                tableName = defaultTableName;
            }
            try
            {
                CloudTableClient tableClient = storageAccount.CreateCloudTableClient();
                CloudTable table = tableClient.GetTableReference(tableName);
                await table.CreateIfNotExistsAsync().ConfigureAwait(false);
                await table.ExecuteAsync(TableOperation.Insert(entity)).ConfigureAwait(false);
            }
            catch { throw; }
        }

    }
}
