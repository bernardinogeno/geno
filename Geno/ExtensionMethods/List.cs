﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geno.ExtensionMethods
{
    public static class CollectionExtensions
    {
        public static bool IsNotNullOrEmpty<T>(this ICollection<T> data)
        {
            if (data == null)
            {
                return false;
            }
            if (data.Count < 1)
            {
                return false;
            }
            return true;
        }

        public static bool IsNullOrEmpty<T>(this ICollection<T> collection)
        {
            return !collection.IsNotNullOrEmpty();
        }
    }
}
