﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;


[assembly: CLSCompliant(false)]
namespace Geno.Net
{
    public static class Web
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "0#", Justification = "It is intended to be used as string")]
        public async static Task<string> GetWebUrlResponseAsync(string url, int timeout = -1)
        {
            try
            {
                string result = "";
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                if (timeout > 0)
                {
                    myRequest.Timeout = timeout;
                }
                using (HttpWebResponse response = (HttpWebResponse)await myRequest.GetResponseAsync())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string htmlResponse = "";
                        using (System.IO.StreamReader streamReader = new StreamReader(response.GetResponseStream(), true))
                        {
                            htmlResponse = streamReader.ReadToEnd().ToString();
                        }
                        result = htmlResponse;
                    }
                }
                return result;
            }
            catch
            {
                throw;
            }
        }

        public async static Task<string> GetWebUrlResponseAsync(string url, string method, int timeout = -1)
        {
            string result = "";
            HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
            if (method.ToUpper().Equals("POST"))
            {
                myRequest.ContentLength = 0;
            }
            myRequest.Method = method;
            if (timeout > 0)
            {
                myRequest.Timeout = timeout;
            }

                using (HttpWebResponse response = (HttpWebResponse)await myRequest.GetResponseAsync())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string htmlResponse = "";
                        using (System.IO.StreamReader streamReader = new StreamReader(response.GetResponseStream(), true))
                        {
                            htmlResponse = streamReader.ReadToEnd().ToString();
                        }
                        result = htmlResponse;
                    }
                }
            
            return result;
        }

        public static string GetWebUrlResponse(string url, string method, int timeout = -1)
        {
            try
            {
                string result = "";
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = method;
                if (timeout > 0)
                {
                    myRequest.Timeout = timeout;
                }
                using (HttpWebResponse response = (HttpWebResponse)myRequest.GetResponse())
                {
                    if (response.StatusCode == HttpStatusCode.OK)
                    {
                        string htmlResponse = "";
                        using (System.IO.StreamReader streamReader = new StreamReader(response.GetResponseStream(), true))
                        {
                            htmlResponse = streamReader.ReadToEnd().ToString();
                        }
                        result = htmlResponse;
                    }
                }
                return result;
            }
            catch
            {
                throw;
            }
        }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "0#", Justification = "It is intended to be used as string")]
        public async static Task<HttpStatusCode> GetWebUrlResponseStatusCodeAsync(string url)
        {
            try
            {
                HttpWebRequest myRequest = (HttpWebRequest)WebRequest.Create(url);
                using (HttpWebResponse response = (HttpWebResponse)await myRequest.GetResponseAsync())
                {
                    return response.StatusCode;
                }
            }
            catch
            {
                throw;
            }
        }

        public static string GetRequestorIPAddress(HttpRequest httpRequest)
        {
            if (httpRequest == null)
            {
                throw new ArgumentNullException("httpRequest", "Parameter httpRequest should not be null");
            }
            string ip = httpRequest.ServerVariables["HTTP_X_FORWARDED_FOR"];
            string[] ipTemp;
            int count;
            if (!string.IsNullOrEmpty(ip))
            {
                string[] ipRange = ip.Split(',');
                ip = ipRange[0];
            }
            else
            {
                ip = httpRequest.ServerVariables["REMOTE_ADDR"];
            }
            ipTemp = ip.Split(':');
            count = ipTemp.Count();
            if (count > 1)
            {
                ip = ipTemp[0];
            }
            return ip;
        }

    }
}