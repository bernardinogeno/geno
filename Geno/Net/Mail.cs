﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Geno.Net
{
    public static class Mail
    {
        public static async Task SendEmailAsync(string host, string username, string password, string sender, string recipient, string replyTo, Collection<string> bcc, bool isBodyHtml, string subject, string body)
        {
            try
            {
                using (var smtpClient = new SmtpClient())
                using (var mailMessage = new MailMessage(sender, recipient))
                {
                    smtpClient.Host = host;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                    smtpClient.UseDefaultCredentials = false;
                    smtpClient.Port = 587;
                    smtpClient.Credentials = new NetworkCredential(username, password);
                    mailMessage.IsBodyHtml = isBodyHtml;
                    mailMessage.Subject = subject;
                    mailMessage.Body = body;

                    if (replyTo != null)
                    {
                        mailMessage.ReplyToList.Add(replyTo);
                    }
                    if (bcc != null)
                    {
                        foreach (var email in bcc)
                            mailMessage.Bcc.Add(email);
                    }
                    await smtpClient.SendMailAsync(mailMessage);
                }
            }
            catch
            {
                throw;
            }
        }
    }
}
