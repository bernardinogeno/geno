﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Geno.Azure
{


    public sealed class Table
    {
        CloudStorageAccount cloudStorageAccount;
        CloudTableClient tableClient;
        CloudTable cloudTable;

        private Table()
        {
        }

        private async Task InitializeAsync(string connectionString, string tableName)
        {
            cloudStorageAccount = CloudStorageAccount.Parse(connectionString);
            tableClient = cloudStorageAccount.CreateCloudTableClient();
            cloudTable = tableClient.GetTableReference(tableName);
            await cloudTable.CreateIfNotExistsAsync();
        }

        public static async Task<Table> CreateAsync(string connectionString, string tableName)
        {
            if (string.IsNullOrEmpty(connectionString))
            {
                throw new ArgumentNullException("connectionString");
            }
            Table result = new Table();
            await result.InitializeAsync(connectionString, tableName);
            return result;
        }

        public async Task<T> RetrieveRow<T>(string partitionKey, string rowKey)
            where T : class, ITableEntity
        {
            TableOperation retrieveOperation = TableOperation.Retrieve<T>(partitionKey, rowKey);

            TableResult retrievedResult = await cloudTable.ExecuteAsync(retrieveOperation);

            return retrievedResult.Result as T;
        }

    }
}
